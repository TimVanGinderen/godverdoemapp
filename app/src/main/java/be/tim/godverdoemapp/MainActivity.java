package be.tim.godverdoemapp;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.CompositePermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener;
import com.obsez.android.lib.filechooser.ChooserDialog;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

// TODO:
// - Settings:
//             - match number field(s) (contact picker)
//             - match sms contents field
// - meerdere nummers (aan meerdere sounds kopplen 3 voor normal oneindig voor gold version)


enum AppStatus { RUNNING, STOPPED}

public class MainActivity extends AppCompatActivity {
    public static final String  TAG = "GVDM.MainActivity";

    public static final String MY_PREFS_NAME = "GodverdoemappPrefsFile";
    public static final String PREF_KEY_PHONE_NR = "PREF_KEY_PHONE_NR";
    public static final String PREF_KEY_VOLUME = "PREF_KEY_VOLUME";
    public static final String PREF_KEY_SOUND_FILE = "PREF_KEY_SOUND_FILE_PATH";
    public static final String PREF_KEY_APP_STATUS = "PREF_KEY_APP_STATUS";
    public static final String PREF_KEY_TTS = "PREF_KEY_TTS";

    FrameLayout root;
    ToggleButton tBtnAppStatus;
    TextView txtAppStatus;
    ImageView ivBartDw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        root = (FrameLayout) findViewById(R.id.root);
        final EditText etxtPhoneNumber = (EditText) findViewById(R.id.etxt_phone_number);
        final SeekBar seekBarVolume = (SeekBar) findViewById(R.id.seekbar_volume);
        final TextView txtVolume = (TextView) findViewById(R.id.txt_volume);
        final TextView txtMedia = (TextView) findViewById(R.id.txt_media);
        final Button btnChooseMedia = (Button) findViewById(R.id.btn_choose_media);
        final Button btnResetMedia = (Button) findViewById(R.id.btn_reset_media);
        tBtnAppStatus = (ToggleButton) findViewById(R.id.tbtn_app_status);
        txtAppStatus = (TextView) findViewById(R.id.txt_app_status);
        final Switch switchEnableTts = (Switch) findViewById(R.id.switch_enable_tts);
        ivBartDw = (ImageView) findViewById(R.id.iv_bartdw);

        etxtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Save number to shared prefs
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString(PREF_KEY_PHONE_NR, charSequence.toString());
                editor.apply();
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        seekBarVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txtVolume.setText(""+i);

                // Save number to shared prefs
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putInt(PREF_KEY_VOLUME, i);
                editor.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        btnChooseMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ChooserDialog(MainActivity.this)
                        .withFilter(false, false, "mp3", "aac", "flac", "mid", "xmf", "mp4", "m4a", "wav", "ogg")
                        .withStartFile(Environment.getExternalStorageDirectory().getAbsolutePath())
                        // to handle the result(s)
                        .withChosenListener(new ChooserDialog.Result() {
                            @Override
                            public void onChoosePath(String path, File pathFile) {
                                String logText = "Chosen file path: " + path;

                                // Save file path to shared prefs
                                saveMedia(pathFile.toString());

                                // Set label
                                txtMedia.setText(pathFile.getName());

                                Log.i(TAG, logText);
                            }
                        })
                        .build()
                        .show();
            }
        });

        SharedPreferences prefs = getSharedPreferences(MainActivity.MY_PREFS_NAME, MODE_PRIVATE);

        // Get phone number from shared prefs
        String phoneNumber = prefs.getString(MainActivity.PREF_KEY_PHONE_NR, null);
        Log.i(TAG, "Prefs:: phone-nr: " + phoneNumber);

        if (phoneNumber != null) {
            etxtPhoneNumber.setText(phoneNumber);
        }

        // Get volume from shared prefs
        int volume = prefs.getInt(PREF_KEY_VOLUME, Integer.MIN_VALUE);
        Log.i(TAG, "Prefs:: volume: " + volume);

        if (volume == Integer.MIN_VALUE) {
            volume = MediaPlayerService.DEFAULT_VOLUME;
            Log.i(TAG, "Volume not set, going with default: " + volume);
        }

        txtVolume.setText("" + volume);
        seekBarVolume.setProgress(volume);

        // Get selected audio file from shared prefs
        String filePath = prefs.getString(PREF_KEY_SOUND_FILE, null);
        Log.i(TAG, "Prefs:: media: " + filePath);
        if (filePath != null) {
            txtMedia.setText(new File(filePath).getName());
            btnResetMedia.setVisibility(View.VISIBLE);
            btnResetMedia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    saveMedia(null);
                    txtMedia.setText(R.string.default_media_label);
                }
            });
        }

        // Get app status from shared prefs
        int appStatusOrdinal = prefs.getInt(PREF_KEY_APP_STATUS, Integer.MIN_VALUE);

        if (appStatusOrdinal == Integer.MIN_VALUE) {
            // App is running for first time, put status in running mode:
            saveAppStatus(AppStatus.RUNNING);
            setRunningMode();
        } else {
            // Get status and update UI
            if (appStatusOrdinal > AppStatus.values().length-1) {
                // Status not found, put app in running mode (should never reach this)
                saveAppStatus(AppStatus.RUNNING);
                setRunningMode();
            } else {
                final AppStatus status = AppStatus.values()[appStatusOrdinal];
                switch (status) {
                    case RUNNING:
                        setRunningMode();
                        break;
                    case STOPPED:
                        setStopMode();
                        break;
                }
            }
        }

        tBtnAppStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.i(TAG, "Set app status to running");
                    saveAppStatus(AppStatus.RUNNING);
                    txtAppStatus.setText(R.string.app_status_running);
                } else {
                    Log.i(TAG, "Set app status to false");
                    saveAppStatus(AppStatus.STOPPED);
                    txtAppStatus.setText(R.string.app_status_stopped);
                }
            }
        });

        // Get tts value from shared prefs
        final Boolean isTTSEnabled = prefs.getBoolean(MainActivity.PREF_KEY_TTS, false);
        Log.i(TAG, "Prefs:: tts enabled? : " + isTTSEnabled);

        switchEnableTts.setChecked(isTTSEnabled);

        // Save tts value on switch change
        switchEnableTts.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveIsTTSEnabled(isChecked);
            }
        });

        // Check sms permissions
        checkSmsPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkServiceStatus();

        // Turn app in stopped mode if SMS permission not granted by now
        final String requiredPermission = "android.permission.RECEIVE_SMS";
        int checkVal = getApplicationContext().checkCallingOrSelfPermission(requiredPermission);
        if (checkVal != PackageManager.PERMISSION_GRANTED){
            setStopMode();
            saveAppStatus(AppStatus.STOPPED);
            // Over-write status text
            txtAppStatus.setText("SMS permissions not enabled, app stopped");
        } else {
            resetAppStatus();
        }
    }

    private void checkSmsPermission() {
        Log.i(TAG,"checkSmsPermission() start");
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.RECEIVE_SMS)
                .withListener(new CompositePermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Log.i(TAG, "onPermissionGranted");

                        // Set app in running mode
                        setRunningMode();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Log.i(TAG, "onPermissionDenied");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        Log.i(TAG, "onPermissionRationaleShouldBeShown");
                    }
                },
                SnackbarOnDeniedPermissionListener.Builder.with(root,
                        R.string.sms_permission_denied_feedback)
                        .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                        .withCallback(new Snackbar.Callback() {
                            @Override
                            public void onShown(Snackbar snackbar) {
                                super.onShown(snackbar);
                            }

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                super.onDismissed(snackbar, event);
                            }
                        })
                        .withDuration(Snackbar.LENGTH_INDEFINITE)
                        .build()))
                .check();
    }

    private void checkServiceStatus() {
        if (MediaPlayerService.IS_PLAYING) {
            // Roteer Bartje
            RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

            //Setup anim with desired properties
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(Animation.INFINITE); //Repeat animation indefinitely
            anim.setDuration(700); //Put desired duration per anim cycle here, in milliseconds

            //Start animation
            ivBartDw.startAnimation(anim);

            //Check for stop
            checkServiceEverySecondForStop();
        } else {
            ivBartDw.setAnimation(null);
        }
    }

    private void checkServiceEverySecondForStop() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!MediaPlayerService.IS_PLAYING) {
                    ivBartDw.setAnimation(null);
                }
            }
        }, 0, 1000);
    }

    private void resetAppStatus() {
        SharedPreferences prefs = getSharedPreferences(MainActivity.MY_PREFS_NAME, MODE_PRIVATE);
        int appStatusOrdinal = prefs.getInt(PREF_KEY_APP_STATUS, Integer.MIN_VALUE);

        final AppStatus status = AppStatus.values()[appStatusOrdinal];
        switch (status) {
            case RUNNING:
                setRunningMode();
                break;
            case STOPPED:
                setStopMode();
                break;

        }
    }

    private void setRunningMode() {
        tBtnAppStatus.setChecked(true);
        txtAppStatus.setText(R.string.app_status_running);
    }

    private void setStopMode() {
        tBtnAppStatus.setChecked(false);
        txtAppStatus.setText(R.string.app_status_stopped);
    }

    private void saveAppStatus(AppStatus status) {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(PREF_KEY_APP_STATUS, status.ordinal());
        editor.apply();
    }
    private void saveMedia(String pathFile) {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(PREF_KEY_SOUND_FILE, pathFile);
        editor.apply();
    }
    private void saveIsTTSEnabled(boolean isEnabled) {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putBoolean(PREF_KEY_TTS, isEnabled);
        editor.apply();
    }
}
