package be.tim.godverdoemapp;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.io.IOException;
import java.util.Locale;

public class MediaPlayerService extends Service implements MediaPlayer.OnPreparedListener {
    private static final String TAG = "GVDM.MediaPlayerService";

    public static final String ACTION_PLAY = "be.tim.action.PLAY";
    public static final String ACTION_STOP = "be.tim.action.STOP";
    public static final String EXTRA_SMS_MESSAGE = "be.tim.extra.SMS_MESSAGE";

    public static final int DEFAULT_VOLUME = 5; // 0-20

    MediaPlayer mediaPlayer = null;

    public static boolean IS_PLAYING = false;

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand start" );
        if (intent.getAction().equals(ACTION_PLAY)) {
            IS_PLAYING = true;

            SharedPreferences prefs = getSharedPreferences(MainActivity.MY_PREFS_NAME, MODE_PRIVATE);

            String smsMessage = intent.getStringExtra(EXTRA_SMS_MESSAGE);

            // Play text to speech
            final boolean isTTSEnabled = prefs.getBoolean(MainActivity.PREF_KEY_TTS, false);

            if (isTTSEnabled) {
                Log.i(TAG, "Start playing text to speech: " + smsMessage);
                playTextToSpeech(getApplicationContext(), smsMessage);
            } else {
                Log.i(TAG, "TTS disabled");
            }

            // Start vibrating
            vibratePhone();

            // init media-player
            mediaPlayer = new MediaPlayer();

            // load sound asset
            try {
                // Check if custom sound is chosen, load default mp3 otherwise
                String filePath = prefs.getString(MainActivity.PREF_KEY_SOUND_FILE, null);

                if(filePath != null) {
                    mediaPlayer.setDataSource(filePath);
                } else {
                    AssetFileDescriptor descriptor = getAssets().openFd("alongcomesmary.mp3");
                    mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                    descriptor.close();
                }
            } catch(IOException e) {
                Log.e(TAG, "Exception while trying to open sound asset", e);
            }

            // Get volume from shared prefs
            int volume = prefs.getInt(MainActivity.PREF_KEY_VOLUME, Integer.MIN_VALUE);

            if (volume == Integer.MIN_VALUE) {
                volume = DEFAULT_VOLUME;
            }

            // set volume
            AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, volume, 0);

            if (Build.VERSION.SDK_INT >= 21) {
                mediaPlayer.setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .build());
            } else {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            }

            // loop sound
            mediaPlayer.setLooping(true);

            // set listener
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.prepareAsync(); // prepare async to not block main thread
        } else if (intent.getAction().equals(ACTION_STOP)) {
            IS_PLAYING = false;

            // Stop vibrating
            stopVibratePhone();

            // stop media playback
            if (mediaPlayer != null) {
                mediaPlayer.stop();
            }

            // hide notification
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(MessageReceiver.NOTIFICATION_ID);
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /** Called when MediaPlayer is ready */
    public void onPrepared(MediaPlayer player) {
        player.start();
    }

    TextToSpeech tts;
    private void playTextToSpeech(Context context, String text) {
        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    int result = tts.setLanguage(Locale.US);
                    if(result==TextToSpeech.LANG_MISSING_DATA ||
                            result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("error", "This Language is not supported");
                    }
                    else{
                        ConvertTextToSpeech(text);
                    }
                }
                else {
                    Log.e("error", "Initilization Failed!");
                }
            }
        });
    }

    private void ConvertTextToSpeech(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void vibratePhone() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // 0 : Start without a delay
        // 400 : Vibrate for 400 milliseconds
        // 200 : Pause for 200 milliseconds
        // 400 : Vibrate for 400 milliseconds
        long[] mVibratePattern = new long[]{0, 400, 200, 400};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createWaveform(mVibratePattern, 0));
//       TODO implement amplituteds     public static VibrationEffect createWaveform(long[] timings, int[] amplitudes, int repeat) {
            } else {
            //deprecated in API 26
            // -1 : Do not repeat this pattern
            // pass 0 if you want to repeat this pattern from 0th index
            v.vibrate(mVibratePattern, 0);
        }
    }

    private void stopVibratePhone() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.cancel();
    }
}