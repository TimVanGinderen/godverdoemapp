package be.tim.godverdoemapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;

public class MessageReceiver extends BroadcastReceiver {

    private static final String TAG = "GVDM.MessageReceiver";

    private static final String CHANNEL_ID = "be.tim.godverdoemapp.notify_001";
    public static final int NOTIFICATION_ID = 1;

    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs = context.getSharedPreferences(MainActivity.MY_PREFS_NAME, context.MODE_PRIVATE);

        // Check if app is in RUNNING mode before going any further
        int status = prefs.getInt(MainActivity.PREF_KEY_APP_STATUS, -1);
        if (status != AppStatus.RUNNING.ordinal()) {
            Log.i(TAG, "App status: " + status);
            Log.i(TAG, "App is not running, don't inspect message");
            return;
        }
        final Bundle pudsBundle = intent.getExtras();
        final Object[] pdus = (Object[]) pudsBundle.get("pdus");
        final SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        final String messageBody = messages.getMessageBody();
        Log.i(TAG, messageBody);
        Log.i(TAG, messages.getOriginatingAddress());

        // Get phone number from shared prefs
        String phoneNumber = prefs.getString(MainActivity.PREF_KEY_PHONE_NR, null);

        if (phoneNumber == null) {
            return;
        }

        // Check if message nr matches the one from shared prefs
        if (messages.getOriginatingAddress().contains(phoneNumber)) {
            Log.i(TAG, "Contains " + phoneNumber);
            showNotification(context, messageBody);
            playSound(context, messageBody);
        }
    }

    private void playSound(Context context, String smsMessage) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(MediaPlayerService.ACTION_PLAY);
        intent.putExtra(MediaPlayerService.EXTRA_SMS_MESSAGE, smsMessage);
        context.startService(intent);
    }

    private void showNotification(Context context, String smsMessage) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(MediaPlayerService.ACTION_STOP);
        int requestID = (int) System.currentTimeMillis();

        PendingIntent pendingIntent = PendingIntent.getService(context, requestID, intent, 0);

        // Build notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(smsMessage)
                .setAutoCancel(false)
                .addAction(R.drawable.ic_launcher_background, context.getString(R.string.action_stop),
                        pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX);


        // Issue the notification
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(CHANNEL_ID);
        }

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;

        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
